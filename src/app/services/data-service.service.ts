import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { Directories } from '../directories';
@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  __directores = new Directories();

  constructor(private http: HttpClient) { }

  getAllProducts(): Observable<any> {
    return this.http.get(this.__directores.getAllData)
      .pipe(retry(5)) // retry 5 times in case of failure or network error.
  }
}
