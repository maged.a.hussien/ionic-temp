import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../Models/Product';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  totalPrice: BehaviorSubject<number> = new BehaviorSubject(0);
  itemsInCart: BehaviorSubject<number> = new BehaviorSubject(0);
  shoppingCart: BehaviorSubject<Product[]> = new BehaviorSubject([]);

  // Getters
  getTotalPrice() {
    return this.totalPrice;
  }

  getItemsInCart() {
    return this.itemsInCart;
  }

  getShoppingCart() {
    return this.shoppingCart;
  }


  // Setters
  setTotalPrice(message: number) {
    this.totalPrice.next(message);
  }

  setItemsInCart(message: number) {
    this.itemsInCart.next(message);
  }

  // Functionality

  updateNumberOfItemsInCart(incrementOrDecrement: 'increment' | 'decrement') {
    let currentNumberOfItems: number = this.getItemsInCart().value;
    if (incrementOrDecrement === 'increment') {
      this.setItemsInCart(++currentNumberOfItems);
    } else {
      this.setItemsInCart(--currentNumberOfItems);
    }
  }

  updatePrice(incrementOrDecrement: 'increment' | 'decrement', product) {
    let totalPrice = this.getTotalPrice().value;
    if (incrementOrDecrement === 'increment') {
      totalPrice += product.price;
    } else {
      totalPrice -= product.price;
    }
    this.setTotalPrice(totalPrice);
  }

  addToShoppingCart(product: Product) {
    this.updateNumberOfItemsInCart('increment');
    this.updatePrice('increment', product);
    let shoppingCart = this.getShoppingCart().value;
    shoppingCart.push(product);
    this.shoppingCart.next(shoppingCart);
  }

  removeFromCart(product: Product) {
    this.updateNumberOfItemsInCart('decrement');
    this.updatePrice('decrement', product);
    let shoppingCart = this.getShoppingCart().value;
    let indexOfRemovedProduct = shoppingCart.indexOf(product);
    shoppingCart.splice(indexOfRemovedProduct, 1);
    this.shoppingCart.next(shoppingCart);
  }
}
