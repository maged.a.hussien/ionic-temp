import { Router } from "@angular/router";
import {
    LoadingController,
    ModalController,
    AlertController,
} from "@ionic/angular";
import { ToastController } from "@ionic/angular";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class UIService {
    actionType: any = 1; // means approve, reject
    modal: any;
    lang: any;

    constructor(
        public loadingController: LoadingController,
        public toastController: ToastController,
        public alert: AlertController,
    ) {
    }

    public async presentLoader() {
        const loader = await this.loadingController.create({
            message: '<img src="assets/imgs/ripple.svg" />',
            translucent: true,
            showBackdrop: true,
            spinner: null,
        });
        loader.present();
        return loader;
    }
}
