import { Component } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { DataServiceService } from '../../services/data-service.service';
import { Category } from '../../Models/Category';
import { Product } from '../../Models/Product';
import { StoreService } from '../../services/store.service';
import { UIService } from '../../services/ui.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  categories: Category[] = [
    {
      id: 1,
      name: 'لبن',
      img_url: 'assets/imgs/milk.png'
    },
    {
      id: 2,
      name: 'عسل',
      img_url: 'assets/imgs/honey.png'
    },
    {
      id: 3,
      name: 'عصير',
      img_url: 'assets/imgs/juice.png'
    },
    {
      id: 4,
      name: 'ايس كريم',
      img_url: 'assets/imgs/ice-cream.png'
    }
  ]

  products: Product[] = [];

  totalPrice$: Observable<number>;
  itemsInCart$: Observable<number>;

  allProductsSubscription: Subscription;
  loader: HTMLIonLoadingElement;
  loaded: boolean = false;

  constructor(private dataService: DataServiceService, private store: StoreService, private ui: UIService) {
    this.totalPrice$ = this.store.getTotalPrice();
    this.itemsInCart$ = this.store.getItemsInCart();
  }

  ionViewWillEnter() {
    this.getAllProducts();
  }

  ionViewWillLeave() {
    this.allProductsSubscription.unsubscribe();
  }

  async getAllProducts() {
    this.loader = await this.ui.presentLoader();
    this.allProductsSubscription = this.dataService.getAllProducts().subscribe((success: { message: string, success: string, result: Product[] }) => {
      this.products = success.result;
      this.loader.dismiss();
      this.loaded = true;
    }, error => {
      console.log(error);
    })
  }

}
