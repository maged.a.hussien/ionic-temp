import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { ProductCardComponent } from '../products/product-card/product-card.component';
import { ArabicNumbersPipe } from '../../pipes/arabic-numbers.pipe';
import { ProductQuantityComponent } from '../products/product-quantity/product-quantity.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, ProductCardComponent, ArabicNumbersPipe, ProductQuantityComponent],
  exports: [ProductCardComponent]
})
export class HomePageModule { }
