import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../Models/Product';
import { StoreService } from '../../../services/store.service';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {

  @Input()
  product: Product;
  showActions: boolean = false;

  constructor(private store: StoreService) { }

  ngOnInit() {
  }

  addToCart() {
    this.store.addToShoppingCart(this.product)
    this.showActions = true;
  }

  showActionButtons(event) {
    if (event === 0) {
      this.showActions = false;
    } else {
      this.showActions = true;
    }
  }

}
