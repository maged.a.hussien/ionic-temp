import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { Product } from '../../../Models/Product';
import { StoreService } from '../../../services/store.service';

@Component({
  selector: 'product-quantity',
  templateUrl: './product-quantity.component.html',
  styleUrls: ['./product-quantity.component.scss'],
})
export class ProductQuantityComponent implements OnInit {
  quantity: number;

  @Input('product') product: Product;

  @Output('quantity')
  empty: EventEmitter<number> = new EventEmitter<number>();

  constructor(private store: StoreService) { }

  ngOnInit() {
    this.getProductQuantity();
  }

  addToCart() {
    this.store.addToShoppingCart(this.product);
  }

  removeFromCart() {
    this.store.removeFromCart(this.product);
  }

  getProductQuantity() {
    this.store.getShoppingCart().subscribe(cart => {
      this.quantity = cart.filter(item => item.id === this.product.id).length;
      this.empty.emit(this.quantity);
    })
  }

}
