export interface Category {
    id: number;
    name: string;
    img_url: string;
}